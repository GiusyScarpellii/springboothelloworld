package com.corso.lez18.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.corso.lez18.models.Persona;


@RestController
public class Task {

	/**
     * Creare un controller che prenda in input:
     * - Un utente e ne restituisce i dettagli
     * - Più utenti e ne restituisce l'elenco
     * - Un metodo che mi conta quanti studenti gli ho passato
     *   sotto forma di elenco e mi restituisce solo il numero
     */
	
	/*
	 * Metodo che consente di inserire un utente nel body
	 * e ne restituisce i dettagli
	 */
	@PostMapping("/dettagli")
	public String restituisciDettagli(@RequestBody Persona temp) {
		String stringa = "";
		stringa += "Nome: " + temp.getNome() + ", Cognome: " + temp.getCognome();
		
		return "L'utente da te inserito è: \n" + stringa;
	}

	/*
	 * Metodo che prende in input un elenco di utenti e che
	 * restituisce l'elenco e il numero di utenti nell'elenco
	 */
	@PostMapping("/elenco")
	public String restituisciNumeroUtenti(@RequestBody List<Persona> elenco){
		
		return elenco + "\n" + "Il numero di utenti da te inserito è: " + elenco.size();
	}
}
